<?php

/**
 * Created by PhpStorm.
 * User: DanielStange
 * Date: 15.01.16
 * Time: 23:28
 */
class Residence_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_residences($pid = 0)
    {
        if ($pid === 0)
        {
            $query = $this->db->get('Residence');
            return $query->result_array();
        }

        $this->db->distinct();
        $this->db->select('PersonResidenceMM.FromDate, PersonResidenceMM.ToDate, Residence.Street, Residence.City, Residence.Street');
        $this->db->from('PersonResidenceMM');
        $this->db->join('Residence', 'Residence.ResidenceId = PersonResidenceMM.ResidenceId', 'left');
        $this->db->where('PersonResidenceMM.PersonId', $pid);
        $this->db->order_by('PersonResidenceMM.FromDate', 'asc');
        $query = $this->db->get();

        return $query->result_array();
    }


}
