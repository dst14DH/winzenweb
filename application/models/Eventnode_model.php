<?php

/**
 * Created by PhpStorm.
 * User: DanielStange
 * Date: 15.01.16
 * Time: 23:28
 */
class Eventnode_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_eventnode($pid = 0)
    {

        $this->db->select('EventNodes.SubjectId, EventNodes.EventId, TimeEvent.TimeEvent.Id, TimeEvent.DateStamp');
        $this->db->from('EventNodes');
        $this->db->join('TimeEvent','TimeEvent.TimeEventId = EventNodes.EventNodeId');

        if ($pid !== 0)
        {
            $this->db->where('EventNodeId', $pid);
        }
        $this->db->order_by('TimeEvent.DateStamp','asc');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_eventnodesforperson($pid = 0)
    {
        $this->db->distinct();
        $this->db->select('EventNodes.EventId, TimeEvent.DateStamp, TimeEvent.Type');
        $this->db->from('EventNodes');
        $this->db->join('TimeEvent','TimeEvent.TimeEventId = EventNodes.EventId','left');

        if ($pid !== 0)
        {
            $this->db->where('EventNodes.SubjectId', $pid);
        }
        $this->db->order_by('TimeEvent.DateStamp','asc');
        $query = $this->db->get();

        return $query->result_array();

    }
}
