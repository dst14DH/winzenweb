<?php

/**
 * Created by PhpStorm.
 * User: DanielStange
 * Date: 15.01.16
 * Time: 23:28
 */
class Person_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_person($pid = 0)
    {
        if ($pid === 0)
        {
            $query = $this->db->get('Person');
            return $query->result_array();
        }
        $query = $this->db->get_where('Person', array('PersonId' => $pid));
        return $query->row_array();
    }


}
