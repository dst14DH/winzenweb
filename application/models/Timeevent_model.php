<?php

/**
 * Created by PhpStorm.
 * User: DanielStange
 * Date: 15.01.16
 * Time: 23:28
 */
class Timeevent_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_timeevent($pid = 0)
    {
        if ($pid === 0) {
            $query = $this->db->get('TimeEvent');
            return $query->result_array();
        }

        $query = $this->db->get_where('TimeEvent', array('TimeEventId' => $pid));
        return $query->row_array();
    }
}
