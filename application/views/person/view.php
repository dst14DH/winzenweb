<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title; ?></title>

<!-- Bootstrap -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<style>
    .starter-template {
        padding-top: 50px;
    }
</style>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Die Datenbank zum Winzen-Gruppen-Projekt</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="http://winzen.hypotheses.org/">Winzen-Gruppe-Blog von Christian Günther</a></li>
                <li><a href="http://blog.danielstange.de/">Blog von Daniel Stange</a></li>
                <li><a href="http://http://blog.danielstange.de/impressum/">Impressum</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">

    <div class="starter-template">

<h1><?php echo trim($person_item['FirstName']." ".$person_item['LastName']); ?></h1>
<div>
    <p>
        <i>Vorname:</i> <?php echo $person_item['FirstName']; ?><br/>
        <i>Nachname:</i> <?php echo $person_item['LastName']; ?><br/>
        <i>Geburtsdatum:</i> <?php
        $date = new DateTime($person_item['BirthDate']);
        echo date_format($date, 'd.m.Y');
        ?><br/>
        <i>Geburtsort:</i> <?php echo $person_item['PlaceOfBirth']; ?><br/>
    </p>
    <p>
        <i>Geburtsname:</i> <?php echo $person_item['MaidenName']; ?><br/>
        <i>Deckname:</i> <?php echo $person_item['CoverName']; ?><br/>
        <i>Hinweis zur Person:</i> <?php echo $person_item['NameHint']; ?><br/>
    </p>
    <p>
        <i>Schulbildung:</i> <?php echo $person_item['Eduction']; ?><br/>
        <i>Konfession:</i> <?php echo $person_item['Religion']; ?><br/>
        <i>Beruf:</i> <?php echo $person_item['Profession']; ?><br/>
    </p>
</div>

<p>
    <a href="<?php echo site_url('person'); ?>">Zurück zur Listenansicht</a>
</p>
    </div>
    <div id="events">
        <?php

        if (count($person_events) != 0) {
            echo "<h2>Wohnsitze</h2>";
        }
        ?>
        <table class="table table-striped">


            <?php

            foreach ($person_residences as $residence):
                ?>
                <tr>
                    <td>
                        <?php
                        if (trim($residence['FromDate']) != '') {
                            $date = new DateTime(trim($residence['FromDate']));
                            echo "Von ";
                            echo date_format($date, 'd.m.Y');
                        }
                       ?>
                    </td>
                    <td>
                        <?php
                        if (trim($residence['ToDate']) != '') {
                            $date = new DateTime(trim($residence['ToDate']));
                            echo "Von ";
                            echo date_format($date, 'd.m.Y');
                        }
                        ?>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="2">
                        <?php
                            $residence_string = $residence['Street'].
                                ' '.
                                $residence['City'].
                                ' '.
                                $residence['District'];

                            echo trim($residence_string);
                        ?>
                    </td>

                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <div id="events">
        <?php

        if (count($person_events) != 0) {
            echo "<h2>Ereignisse</h2>";
        }
        ?>
        <table class="table table-striped">


            <?php

             foreach ($person_events as $event):
                 ?>
                <tr>
                    <td>
                        <?php $date = new DateTime(trim($event['DateStamp']));
                        echo date_format($date, 'd.m.Y');?>
                    </td>
                    <td>
                        <?php echo trim($event['Type']); ?>
                    </td>

                </tr>
            <?php endforeach; ?>
        </table>
    </div>

</div><!-- /.container -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>