<?php

/**
 * Created by PhpStorm.
 * User: DanielStange
 * Date: 15.01.16
 * Time: 23:34
 */
class Person extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('person_model');
        $this->load->model('eventnode_model');
        $this->load->model('residence_model');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $data['person'] = $this->person_model->get_person();
        $data['title'] = 'Winzen Datenbank';

        //$this->load->view('header', $data);
        $this->load->view('person/index', $data);
    }

    public function view($pid = NULL)
    {
        $data['person_item'] = $this->person_model->get_person($pid);
        $data['person_events'] = $this->eventnode_model->get_eventnodesforperson($pid);
        $data['person_residences'] = $this->residence_model->get_residences($pid);

        if (empty($data['person_item']))
        {
            show_404();
        }

        $data['title'] = $data['person_item']['FirstName'];
        $data['title'] += $data['person_item']['LastName'];
        $this->load->view('person/view', $data);

    }
}