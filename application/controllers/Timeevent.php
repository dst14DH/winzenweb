<?php

/**
 * Created by PhpStorm.
 * User: DanielStange
 * Date: 15.01.16
 * Time: 23:34
 */
class Timeevent extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('timeevent_model');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $data['timeevent'] = $this->timeevent_model->get_timeevent();
        $data['title'] = 'Winzen Datenbank';

        //$this->load->view('header', $data);
        $this->load->view('timeevent/index', $data);
    }

    public function view($pid = NULL)
    {
        $data['timeevent_item'] = $this->timeevent_model->get_timeevent($pid);

        if (empty($data['timeevent_item']))
        {
            show_404();
        }

        $data['title'] = $data['timeevent_item']['DateStamp'];
        $data['title'] += $data['timeevent_item']['Type'];

        $this->load->view('timeevent/view', $data);
    }
}